import React from "react";
import Tag from "./Tag";

export default function Card({ title, description, tags }) {
  return (
    <section className="card">
      <h2>{title}</h2>
      <div>{description}</div>
      {tags && tags.map(tag => <Tag key={tag}>{tag}</Tag>)}
    </section>
  );
}
