import React from "react";

export default function Form({ onSubmit }) {
  function splitToArray(tagValues) {
    return tagValues.split(",").map(tag => tag.trim());
  }

  function handleSubmit(event) {
    event.preventDefault();
    const form = event.target;
    onSubmit({
      title: form.title.value,
      description: form.description.value,
      tags: splitToArray(form.tags.value)
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <input name="title" placeholder="Zusammenfassung eintragen" />
      <input name="description" placeholder="Beschreibung einfügen" />
      <input name="tags" />
      <button>Create New Card</button>
    </form>
  );
}
