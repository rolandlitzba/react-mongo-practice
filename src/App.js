import React, { Component } from "react";
import { getCards, postCard, getLocal, setLocal, patchCard } from "./services";
import CardList from "./CardList";
import Form from "./Form";

export default class App extends Component {
  state = {
    cards: getLocal("cards") || []
  };

  componentDidMount() {
    this.loadCards();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.cards !== this.state.cards) {
      setLocal("cards", this.state.cards);
    }
  }

  loadCards() {
    getCards()
      .then(data => this.setState({ cards: data }))
      .catch(error => console.log(error));
  }

  handleSubmit = card => {
    postCard(card)
      .then(card => {
        console.log(card);
        this.setState({ cards: [...this.state.cards, card] });
      })
      .catch(err => console.log(err));
  };

  render() {
    const { cards } = this.state;

    return (
      <main>
        <h1>Cards</h1>
        <Form onSubmit={this.handleSubmit} />
        <CardList cards={cards} />
      </main>
    );
  }
}
