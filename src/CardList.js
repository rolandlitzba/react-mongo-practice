import React from "react";
import Card from "./Card";

export default function CardList({ cards }) {
  return (
    <section className="cardList">
      {cards.map(card => (
        <Card key={card._id} {...card} />
      ))}
    </section>
  );
}
